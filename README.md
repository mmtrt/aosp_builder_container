**Building an container:**
```bash
$ sudo podman build . -f Dockerfile.ubuntu-22.04  -t aosp.build.ubuntu-22.04 --no-cache
$ sudo podman run --rm   -d --name=aosp.build -p 1111:22 -p 60100-60999:60100-60999/udp  aosp.build.ubuntu-22.04
```

**Connecting without ssh:**
```bash
$ sudo podman exec -it aosp.build /bin/bash
```

## Dont forget to do these before making your own changes in dockerfiles!
